export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  router: {
    base: '/'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  

  head: {
    title: '{Dee}Farm - Software House',
    // htmlAttrs: {
    //     lang: 'en'
    // },
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, shrink-to-fit=no' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    script: [
        { src: "assets/js/vendor/jquery-3.6.0.min.js"},
        { src: "assets/js/vendor/modernizr-3.6.0.min.js"},
       
        { src: "assets/js/vendor/waypoints.js"},
        { src: "assets/js/vendor/counterup.js"},
        { src: "assets/js/vendor/slick.js"},
        { src: "https://www.googletagmanager.com/gtag/js?id=UA-235327332-1" , async: true},

        
        { src: "assets/js/vendor/wow.js"},
        { src: "assets/js/vendor/scrollup.js"},
        { src: "assets/js/vendor/smooth.js"},
        { src: "assets/js/vendor/textType.js"},
        { src: "assets/js/main.js?v=2.0" , defer:true , callback: () => { 

                //$(window).on('load', function () {
                    //console.log("ok");
                    $('.preloader').delay(300).fadeOut('slow');
                    $('body').delay(450).css({
                        'overflow': 'visible'
                    });
                //});
         }},
    
    ],
    link: [
        { rel: 'icon', type: 'image/x-icon', href: 'https://dee.farm/assets/imgs/logos/favicon.svg' },
        { rel: 'stylesheet' , href: 'assets/css/animate.min.css?v=2.0' },
        { rel: 'stylesheet' , href: 'assets/css/slick.css?v=2.0' },
        { rel: 'stylesheet' , href: 'assets/css/tailwind-built.css?v=2.0' },
        { rel: 'preconnect' , href: 'https://fonts.googleapis.com' },
        { rel: 'preconnect' , href: 'https://fonts.gstatic.com' },
        { rel: 'stylesheet' , href: 'https://fonts.googleapis.com/css2?family=Mitr&display=swap' },
        { rel: 'apple-touch-icon' , href: 'https://dee.farm/assets/imgs/logos/icon-180.png' , sizes:"180x180" },
        { rel: 'icon', type: 'image/png' , href: 'https://dee.farm/assets/imgs/logos/icon-32.png' },
        { rel: 'icon', type: 'image/png' , href: 'https://dee.farm/assets/imgs/logos/icon-16.png' },
        // { rel: 'manifest' , href: 'https://dee.farm/assets/imgs/logos/manifest.json' },
        // { rel: 'mask-icon' , href: '/docs/5.2/assets/img/favicons/safari-pinned-tab.svg' , color:"#712cf9" },

    ],


},

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [

  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules

  modules: [
    '@nuxtjs/i18n',
  ],
  i18n: {
    nuxtI18n: false,
    detectBrowserLanguage: {
      useCookie: false,
      cookieKey: 'i18n_redirected',
      alwaysRedirect: false, fallbackLocale: '', redirectOn: 'root', useCookie: true, cookieCrossOrigin: false, cookieDomain: null, cookieKey: 'i18n_redirected', cookieSecure: false
      // redirectOn: 'root',  // recommended
    },
    strategy: 'prefix',
    // locales: ['th','en'],
    defaultLocale: 'th',
    lazy: true,
    langDir: 'locales/',
    locales: [
      {
        code: 'en',
        name: 'english',
        file: 'en.json'
      },
      {
        code: 'th',
        name: 'thai',
        file: 'th.json'
      }
    ],
    // vueI18n: {
    //   fallbackLocale: 'en',
    //   fallbackLocale: ['en', 'fr'],
    // }
  },
 

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}







